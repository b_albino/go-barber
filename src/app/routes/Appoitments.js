import { Router } from 'express';
import auth from '../middleware/auth';

import AppointmentsController from '../controllers/AppointmentsController';

const routes = new Router();

routes.use(auth);

routes.post('/appointments', AppointmentsController.store);
routes.get('/appointments', AppointmentsController.index);
routes.delete('/appointments/:id', AppointmentsController.delete);

export default routes;
