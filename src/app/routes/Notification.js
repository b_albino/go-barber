import { Router } from 'express';
import auth from '../middleware/auth';

import NotificationController from '../controllers/NotificationController';

const routes = new Router();

routes.use(auth);
routes.get('/notification', NotificationController.index);
routes.put('/notification/:id', NotificationController.update);

export default routes;
