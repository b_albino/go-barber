import { Router } from 'express';
import multer from 'multer';
import auth from '../middleware/auth';
import multerConfig from '../../config/multer';
import FileController from '../controllers/FileController';

const routes = new Router();
const upload = multer(multerConfig);

routes.use(auth);
routes.post('/files', upload.single('file'), FileController.store);

export default routes;
