import { Router } from 'express';
import auth from '../middleware/auth';

import ScheduleController from '../controllers/ScheduleController';

const routes = new Router();

routes.use(auth);

routes.get('/schedule', ScheduleController.index);

export default routes;
