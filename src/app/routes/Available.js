import { Router } from 'express';
import auth from '../middleware/auth';

import AvailableController from '../controllers/AvailableController';

const routes = new Router();

routes.use(auth);
routes.get('/provider/:providerId/available', AvailableController.index);

export default routes;
