import { Router } from 'express';
import auth from '../middleware/auth';
import UserController from '../controllers/UserController';
import userUpdateValidator from '../validators/UserUpdate';
import userStoreValidator from '../validators/UserStore';

const routes = new Router();

routes.post('/user', userStoreValidator, UserController.store);
routes.use(auth);

routes.put('/user', userUpdateValidator, UserController.update);
routes.get('/user', UserController.index);

export default routes;
