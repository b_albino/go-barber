import { Router } from 'express';
import auth from '../middleware/auth';

import ProviderController from '../controllers/ProviderController';

const routes = new Router();

routes.use(auth);
routes.get('/provider', ProviderController.index);

export default routes;
