import Users from './app/routes/Users';
import Session from './app/routes/Session';
import File from './app/routes/File';
import Provider from './app/routes/Provider';
import Appoitments from './app/routes/Appoitments';
import Schedule from './app/routes/Schedule';
import Notification from './app/routes/Notification';
import Available from './app/routes/Available';

export default [].concat(
  Users,
  Session,
  File,
  Provider,
  Appoitments,
  Schedule,
  Notification,
  Available
);
